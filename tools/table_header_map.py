#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2020 Institute of Software, CAS. All rights reserved.
# oepkgs_build_system_utils is licensed under Mulan PSL v2.
# You can use this software according to the terms and conditions of
# the Mulan PSL v2.
# You may obtain a copy of Mulan PSL v2 at:
# http://license.coscl.org.cn/MulanPSL2
# THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF
# ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
# NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
# See the Mulan PSL v2 for more details.
#
# Original author: Ocean Killer <ocean8192@isrc.iscas.ac.cn>
# Date: 04 Nov 2020

import json
import os


class _const:
    class ConstError(TypeError):
        pass

    def __setattr__(self, name, value):
        if name in self.__dict__:
            raise self.ConstError(f"Can't rebind const {name}")
        self.__dict__[name] = value

def base_path_set(path):
    '''Setting base path to exsiting _const class
    '''
    if not isinstance(path, str):
        raise TypeError('path should be a string')

    _const.tools_path = path

    # Those properties should be initialized after path setting
    with open(os.path.join(path, 'db_header.json'), 'r') as f:
        _const.DB = json.load(f)

    with open(os.path.join(path, 'json_header.json'), 'r') as f:
        _const.JSON = json.load(f)

    with open(os.path.join(path, 'json_db_mapping.json'), 'r') as f:
        _const.J2D = json.load(f)


if __name__ == 'table_header_map':
    pass


if __name__ == '__main__':
    base_path_set(os.getenv('PWD'))

    print('Table header map relationship should be stored in JSON file.')
    print(_const)
    print(type(_const))
    print(_const.DB)
    print(type(_const.DB))
    print(_const.JSON)
    print(type(_const.JSON))
