# oepkgs 软件编译系统配置

## 介绍

本仓库用于 oepkgs 软件编译系统的软件包配置，其中的信息用于被导入到编译系统数据库。每次有新的 packager 或者新的软件包需要编译就可以通过向此软件仓库提交 PR (Pull Request)，来达到通知系统管理员配置系统并提供相应信息的目的。

* 此编译系统管理员，下简称“管理员”。
* 软件包维护者及仓库所有者，提交commit并触发编译的工程师，下简称“工程师”。

## 软件架构简介

![软件架构图](docs/figures/overview_v2.png)

### 系统组成

1. [Gitee](https://gitee.com/)
    * 软件包仓库列表及其配置
    * 软件包仓库

2. 基于 Huawei cloud 的服务器集群
    * Koji server 主服务器[一台虚拟主机]
      * Koji服务
      * Jenkins服务
      * Nginx 反向代理服务
    * Koji Builder[多台虚拟主机]
      * AArch64 builders
      * X86_64 builders
    * NFS 存储服务
      * 开发资源
      * Linux Distros REPOs

### 编译过程简述

1. 工程师通过在Gitee中某个软件包仓库的操作（push、merge等）触发Gitee针对Jenkins的Webhook(需要工程师进行适当配置)。

2. Gitee Webhook触发Jenkins的Gitee Plugin(需要管理员通过工程师提交的配置信息进行任务的创建和配置)

3. Jenkins的Gitee Plugin被激活后会提交相应的编译任务给 Koji Hub（Koji Server中的组件），并开始获取任务状态，直至任务结束。
    * Koji Hub 根据任务信息，将任务分配给相应的空闲Koji Builder进行编译。
    * 如果Koji Builder编译成功，Jenkins负责将软件包复制到仓库所有者指定的软件仓库路径（位于NFS存储服务中），并更新仓库信息。

4. 任务结束后，Jenkins负责收集所有任务信息，并通过Gitee comment反馈给相关仓库的特定 commit 点。

## 配置仓库简介

本配置仓库结构如下：

```bash
├── docs (相关文档)
│   ├── figures (文档中包含的图片)
│   ├── templates (系统配置 json 文件模板)
│   │   ├── package_template.json (软件包编译任务配置信息)
│   │   └── user_template.json (工程师 Gitee 账户相关信息)
│   ├── gitee_webhook_config.md
│   |   (工程师需要仔细阅读的文档， Gitee Webhook 配置文档)
│   └── jenkins_jobs_config.md
│       (管理员需要仔细阅读的文档， Jenkins Jobs 配置文档)
├── keys (编译系统和配置需要用到的加密过的 tokens 和各种公钥)
│   ├── api_token (工程师提供的加密过的Personal access tokens 集合，optional)
│   ├── project_token (管理员提供的加密过的Secret Token for Gitee WebHook 集合)
│   ├── system (编译系统公钥集合，用于让工程师加密 Personal access tokens 给管理员)
│   └── user_pub (工程师的公钥集合，用于让管理员传递加密 Secret Token 给工程师)
├── projects (针对不同发行版的配置文件集合)
│   └── openEuler (软包对应的发行版名)
│       └── 20.03-LTS (软包对应的发行版版本号)
│           └── Packages(软件包编译配置文件)
│               ├── x(软件包首字母)
│               │   └── xz(软件包名)
│               │       └── test_pkg.json(软件包编译配置文件)
│               │            (同一个软件包可以允许多人从不同仓库编译)
│               └── z
│                   └── zlib
│                       └── oepkgs_test.json
├── README.md (本文档)
├── tools (配置信息相关小工具)
│   └── oepkgs_djtools (配置生成辅助工具，oepkgs Database&Json tools )
└── users (工程师 Gitee 账户信息)
    ├── e (工程师用户名首字母)
    │   └── eulerblade.json (某个工程师 Gitee 账户信息文件)
    └── o
        └── ocean-killer.json
```

## 配置仓库使用简述

步骤说明

1. 工程师提出编包申请，提交 commit， 并向 upstream 仓库提交 PR。
  这个 commit 必须包含以下内容：
    * 工程师 Gitee 账户信息文件 (如上面提到的 users/e/eulerblade.json) [如果此次编译申请的提交者此前使用同一账号提交过，则此文件不用重复提交]

    * 软件包编译配置文件 (如上面提到的 projects/OpenEuler/20.03-LTS/Packages/z/zlib/oepkgs_test.json) [不包含 project_token 信息，此由管理员提供]

    * api_token： 使用 *_oepkg.pub 加密过的 Gitee Personal access tokens) 数据文件

    * user_pub： 工程师的公钥数据文件，文件名必须为完整指纹

2. 管理员审核工程师的提交。如果接受，则合并 PR 并再提交一个相关的 commit 到 upstream 仓库。
  这个 commit 必须包含以下内容：
    * 补全软件包编译配置文件中的 "project_token" 和 "WebHook_URL" 信息.

    * project_token: 使用工程师的公钥加密过的 Secret Token for Gitee WebHook 数据文件

3. 管理员使用 tools/config_import 工具将新信息导入编译系统数据库。

4. 工程师将管理员提交的 "project_token" 用自己的私钥解密并连同 "WebHook_URL"，配置 Webhook。

## 关于 keys (各种 token 和 公钥) 的关系说明

![配置文件和各种keys的对应图](docs/figures/json_and_keys.png)

### token 及公钥相关介绍及文件命名规则

├── keys (编译系统和配置需要用到的加密过的 tokens 和各种公钥)
    ├── api_token (工程师提供的加密过的Personal access tokens 集合，optional)
    │   └── 0d (内含文件的文件名前两位，小写字母)
    │       └── 0ddf188c9226811daf58d40e45accb9ca8e98af8 (使用 oepkgs 编译系统公钥加密过的 Secret Token 文件的**sha1sum值**，小写字母)
    ├── project_token (管理员提供的加密过的 Secret Token for Gitee WebHook 集合)
    │   └── 0d (内含文件的文件名前两位，小写字母)
    │       └── 0ddf188c9226811daf58d40e45accb9ca8e98af8 (使用 user 公钥加密过的 Secret Token 文件的**sha1sum值**，小写字母)
    ├── system (编译系统公钥集合，用于让工程师加密 Personal access tokens 给管理员)
    │   └── C31300E8175BF0735D1FF8ADFC7BCFADB768161C_oepkgs.pub (oepkgs编译系统公钥的完整指纹加后缀'_oepkgs.pub'， 指纹部分为大写字母)
    └── user_pub (工程师的公钥集合，用于让管理员传递加密 Secret Token 给工程师)
        ├── 35 (内含文件的文件名前两位，大写字母)
        │   └── 3585F45D3A0DFAC62911BBB3DA6C08D4FA1ECE81 (工程师公钥的**完整指纹**，大写字母)
        └── E0 (内含文件的文件名前两位，大写字母)
            └── E0230505E0D0D2948050CADD964617B09C96FB5E (工程师公钥的**完整指纹**，大写字母)

* 大小写字母问题： 加密后的 token 文件名由 sha1sum 命令生成， 默认是小写字母， 所以采用小写字母; gpg 程序对密钥指纹的输出默认是大写字母， 所以才用大写字母。**目的**：方便使用，不用转换，并可以明显区分 token 和公钥文件。

本项目使用 GPG 加密方式对 token 等敏感信息进行加密， 具体命令如下：

### 如何导出 GPG 公钥

1. 生成 GPG key（如已经生成过，可以跳过）

    ```bash
    [euler_blade@samantha ~]$ gpg --full-gen-key
    gpg (GnuPG) 2.2.20; Copyright (C) 2020 Free Software Foundation, Inc.
    This is free software: you are free to change and redistribute it.
    There is NO WARRANTY, to the extent permitted by law.

    Please select what kind of key you want:
      (1) RSA and RSA (default)
      (2) DSA and Elgamal
      (3) DSA (sign only)
      (4) RSA (sign only)
      (14) Existing key from card
    Your selection?
    RSA keys may be between 1024 and 4096 bits long.
    What keysize do you want? (2048)
    Requested keysize is 2048 bits
    Please specify how long the key should be valid.
            0 = key does not expire
          <n>  = key expires in n days
          <n>w = key expires in n weeks
          <n>m = key expires in n months
          <n>y = key expires in n years
    Key is valid for? (0)
    Key does not expire at all
    Is this correct? (y/N) y

    GnuPG needs to construct a user ID to identify your key.

    Real name: Euler Blade
    Email address: euler_blade@isrc.iscas.ac.cn
    Comment: oepkgs
    You selected this USER-ID:
        "Euler Blade (oepkgs) <euler_blade@isrc.iscas.ac.cn>"

    Change (N)ame, (C)omment, (E)mail or (O)kay/(Q)uit? O
    We need to generate a lot of random bytes. It is a good idea to perform
    some other action (type on the keyboard, move the mouse, utilize the
    disks) during the prime generation; this gives the random number
    generator a better chance to gain enough entropy.
    We need to generate a lot of random bytes. It is a good idea to perform
    some other action (type on the keyboard, move the mouse, utilize the
    disks) during the prime generation; this gives the random number
    generator a better chance to gain enough entropy.
    gpg: key 964617B09C96FB5E marked as ultimately trusted
    gpg: revocation certificate stored as '/home/euler_blade/.gnupg/openpgp-revocs.d/E0230505E0D0D2948050CADD964617B09C96FB5E.rev'
    public and secret key created and signed.

    pub   rsa2048 2020-10-17 [SC]
          E0230505E0D0D2948050CADD964617B09C96FB5E
    uid                      Euler Blade (oepkgs) <euler_blade@isrc.iscas.ac.cn>
    sub   rsa2048 2020-10-17 [E]

    [euler_blade@samantha ~]$

    ```

2. 导出公钥

    ```bash
    [euler_blade@samantha ~]$ gpg --list-keys
    gpg: checking the trustdb
    gpg: marginals needed: 3  completes needed: 1  trust model: pgp
    gpg: depth: 0  valid:   1  signed:   0  trust: 0-, 0q, 0n, 0m, 0f, 1u
    /home/euler_blade/.gnupg/pubring.kbx
    --------------------------------------
    pub   rsa2048 2020-10-17 [SC]
          E0230505E0D0D2948050CADD964617B09C96FB5E
    uid           [ultimate] Euler Blade (oepkgs) <euler_blade@isrc.iscas.ac.cn>
    sub   rsa2048 2020-10-17 [E]

    [euler_blade@samantha ~]$ OEPKGS_KEYID=E0230505E0D0D2948050CADD964617B09C96FB5E
    [euler_blade@samantha ~]$ gpg --armor --output ${OEPKGS_KEYID} --export ${OEPKGS_KEYID}
    [euler_blade@samantha ~]$ ll ${OEPKGS_KEYID}
    -rw-rw-r--. 1 euler_blade euler_blade 1769 Oct 17 20:55 E0230505E0D0D2948050CADD964617B09C96FB5E
    ```

#### 工程师导出的公钥

作为工程师， 此处的‘E0230505E0D0D2948050CADD964617B09C96FB5E’就是可以放入 keys/user_pub/ 的文件。由于这个公钥指纹是以‘E0’开头的，所以这个文件可以放入 keys/user_pub/E0/

```bash
mkdir -p ${oepkgs_packages_configs_dir}/keys/user_pub/E0
mv E0230505E0D0D2948050CADD964617B09C96FB5E ${oepkgs_packages_configs_dir}/keys/user_pub/E0/
```

#### OEPKGS的管理员导出公钥

作为OEPKGS编译系统的管理员，此处的公钥需要添加 _oepkgs.pub 后缀，再放入 keys/system/ 目录。

```bash
PUB_KEY=C31300E8175BF0735D1FF8ADFC7BCFADB768161C
mkdir -p ${oepkgs_packages_configs_dir}/keys/system
mv ${PUB_KEY} ${oepkgs_packages_configs_dir}/keys/system/${PUB_KEY}_oepkgs.pub
```

### 工程师加密 Gitee 的 Personal access tokens（可选）

**注意： 此 token 仅在工程师提交的代码仓库 Property 为 “Private” 的时候才需要！**

#### 使用 shell 命令（原理）

1. 导入 GPG 公钥，并查看 key ID

    ```bash
    # *_oepkgs.pub 代表了由 oepkgs 编译系统提供的公钥，用于让工程师加密 Personal access tokens 给管理员
    OEPKGS_PUB=keys/system/*_oepkgs.pub
    # 导入 oepkgs 编译系统提供的公钥
    gpg --import ${OEPKGS_PUB}
    gpg --list-keys
    # 你可以再这里看到刚才导入的 oepkgs 公钥ID（指纹），此处我们暂且认为将其存入环境变量${OEPKGS_PUB_ID}中。
    ```

2. 加密token

    ```bash
    TOKEN_ENCRYPTED=token_en_tmp.bin
    echo -n '[token_string]' | gpg --encrypt --output ${TOKEN_ENCRYPTED} --recipient ${OEPKGS_PUB_ID}

    mv -vi ${TOKEN_ENCRYPTED} $(sha1sum ${TOKEN_ENCRYPTED}| awk '{print $1;}')
    ```

3. 将文件导入 oepkgs 配置中

将刚刚生成的文件存放到 [keys/api_token/](keys/api_token/) 中，并将文件名存入 users/*/*json 中的 "api_token": 中。

#### 使用 oepkgs_djtools 脚本

在 tools 目录下有一个 oepkgs_djtools 脚本帮你快速的完成这个工作， 使用方法如下:

```bash

[euler_blade@samantha oepkgs_packages_configs]$ tools/oepkgs_djtools apitoken --token '[token_string]' --user eulerblade

```

* --token: 指定 token 字符串。

* --user： 在生成 api_token 目录中的文件之后会更新相应用户的 user.json 文件。

此工具的工作流程如下：

1. 通过提供的 token 字符串和 oepkgs.pub 生成加密过的文件， 文件名为文件数据的 sha1sum。

2. 如果我们在 oepkgs_packages_configs 目录下， 将文件按规范移动到 api_token/xx/ 目录下。
   否则请通过 ‘-C’ 指定 oepkgs_packages_configs 所在的 PATH。

3. 通过 '--user' 参数定位 user.json 文件， 并更新 "api_token" 项。

### 管理员获取 api_token (可选)

**注意： 此 token 仅在工程师提交的代码仓库 Property 为 “Private” 的时候才需要！**

api_token 是工程师用 system/*_oepkgs.pub 加密发送来的，可以通过一下方式获取，以 users/e/eulerblade.json 为例：

```json
{
    "user": "eulerblade",
    "email": "euler_blade@isrc.iscas.ac.cn",
    "api_token": "",
    "user_pub": "E0230505E0D0D2948050CADD964617B09C96FB5ES",
    "repos_prefix": "https://gitee.com/eulerblade"
}
```

加密过的 token 信息位于 keys/api_token/

```bash
API_TOKEN_EN=keys/api_token/??/${api_token_in_user_json}
gpg --decrypt ${API_TOKEN_EN}
```

## JSON 文件的路径和命名规则

### project

所有 project 信息 json 文件都需要放在 projects 目录下，其路径如下：

```bash
 projects/<distro_or_OS_name>/<version>/<software_type>/[first_letter_of_software_name]/<software_name>/<job_name>.json
```

<>： 必须
[]: 可选

* <distro_or_OS_name>： 发行版或者OS名称
* <version>： 发行版或者OS版本号
* <software_type>： 编译的软件类型: Packages, Image, or others.
* [first_letter_of_software_name]: 如果是上面的类型是 Packages, 为了避免一个目录下存放太多的文件，此层为软件包名的第一个字母。
* <software_name>： 软件包名
* <job_name>.json： 任务名， 针对同一个软件包可以有多个编译任务配置（源码仓库，分支，软件包仓库等等）。

## 管理员手册

* [Database 维护与配置](docs/datebase.md)

## 参与贡献

* Ocean8192  <ocean8192@isrc.iscas.ac.cn>

* Euler Blade <euler_blade@isrc.iscas.ac.cn>
