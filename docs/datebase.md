# Database 维护与配置

## 初始化数据库

### 使用 oepkgs_djtools 初始化数据库

在 tools 目录下有一个 oepkgs_djtools 脚本帮你快速的完成这个工作， 使用方法如下:

```bash

[euler_blade@samantha oepkgs_packages_configs]$ tools/oepkgs_djtools initdb -d <oepkgs_database_file_path>

```

子命令为 initdb

* --database/-d: 指定数据库文件路径。

此工具的工作流程如下：

1. 通过提供的数据库文件路径创建空文件， **如果文件已经存在就立即错误退出**。

2. 通过以下操作创建数据库的 projects 和 users 空表。

```bash
[oepkger@oepkgs-koji-hub ~]$ sqlite3 /mnt/storage/repodb/oepkgs.db
SQLite version 3.33.0 2020-08-14 13:23:32
Enter ".help" for usage hints.
sqlite> CREATE TABLE projects (JOB_ID INTEGER PRIMARY KEY AUTOINCREMENT, JOB_NAME TEXT UNIQUE NOT NULL, SCM_REPO TEXT NOT NULL, BRANCH TEXT NOT NULL, PROJ_TOKEN TEXT, YUM_REPO TEXT, USER INTEGER, SCRATCH_BUILD INTEGER DEFAULT 1, WEBHOOK_URL TEXT NOT NULL, BUILD_TAG TEXT, OS TEXT, VERSION TEXT, BUILD_TYPE TEXT, PACKAGE_NAME TEXT);

sqlite> CREATE TABLE users (ID INTEGER PRIMARY KEY AUTOINCREMENT, NAME TEXT UNIQUE NOT NULL, EMAIL TEXT, API_TOKEN TEXT, USER_PUBKEY TEXT, REPO_PREFIX TEXT NOT NULL UNIQUE);

```

## 导入 projects 和 users 数据

```bash

[euler_blade@samantha oepkgs_packages_configs]$ tools/oepkgs_djtools import -t user -j users/e/eulerblade.json -d /mnt/storage/repodb/oepkgs.db
[euler_blade@samantha oepkgs_packages_configs]$ tools/oepkgs_djtools import  -t project -j projects/OpenEuler/20.03-LTS/Packages/z/zlib/oepkgs_test.json -d /mnt/storage/repodb/oepkgs.db

```

## 验证导入的数据

```bash

[euler_blade@samantha oepkgs_packages_configs]$ sqlite3 /mnt/storage/repodb/oepkgs.db
SQLite version 3.30.0 2019-10-04 15:03:17
Enter ".help" for usage hints.
sqlite> .header on
sqlite> .mode colum
sqlite> SELECT * FROM users;
ID          NAME        EMAIL                         API_TOKEN   USER_PUBKEY                               REPO_PREFIX                 
----------  ----------  ----------------------------  ----------  ----------------------------------------  ----------------------------
1           eulerblade  euler_blade@isrc.iscas.ac.cn              E0230505E0D0D2948050CADD964617B09C96FB5E  https://gitee.com/eulerblade
sqlite> SELECT * FROM projects;
JOB_ID      JOB_NAME     SCM_REPO    BRANCH       PROJ_TOKEN  YUM_REPO      USER        SCRATCH_BUILD  WEBHOOK_URL                                               BUILD_TAG            OS          VERSION     BUILD_TYPE  PACKAGE_NAME
----------  -----------  ----------  -----------  ----------  ------------  ----------  -------------  --------------------------------------------------------  -------------------  ----------  ----------  ----------  ------------
1           oepkgs_test  /zlib.git   oepkgs_test              /oepkgs_test  1           1              https://jenkins.oepkgs.net/gitee-project/oepkgs_template  openEuler-20.03-LTS  openEuler   20.03-LTS   Packages    zlib        

```
