# Oepkgs 编译系统： Gitee Webhook 配置指南

* 请先阅读本仓库根目录下的 README.md

* 本文档主要用于指导工程师配置自己的 RPM 仓库以对接 oepkgs 编译系统。

## 仓库 WebHook 配置

![介绍](figures/gitee_webhook_02_01.png)

首先进入需要编译的 rpm 包的源码仓库(1)， (2)Settings --> (3)WebHooks。如果之前没有设置过，则会出现以下情况，则可以点击 Add。

![添加仓库WebHook](figures/gitee_webhook_02_02.png)

4 和 5 都和 Jenkins 端的相关信息(配置最终位于Jenkins任务配置的 Build Triggers 页面），需要编译服务器Jenkins管理员通过返回 相关 package 的配置 Json 获取（参见配置仓库根目录下的 [README.md](../README_zh.md)）：

* **4**: 将管理员提交的软件包配置文件中的 "project_token"， 用自己的私钥解密获得的字符串。

```bash
gpg --decrypt project_token/??/${project_token_in_package_json}
```

* **5**: 位于软件包配置文件中的 "WebHook_URL"。

## 生成 Personal access token (optional)

* 注意：此步骤仅在工程师创建**私有**仓库时才需要

为了保证 Gitee **私有**仓库中的 WebHook 可以和 Jenkins 的 Gitee Plugin 在最后编译结束 comment 最终结果的时候正常通讯， 必须使用 Personal access token， 并正确配置。此时你会发现， comment 的账号不再是公开仓库中使用的 oepkgs_bot，而是你自己的账号。

步骤如图所示：

![右上角账号图标](figures/gitee_webhook_01_01.png)

![进入Personal access tokens](figures/gitee_webhook_01_02.png)

* (1)右上角账号头像 --> (2)**Settings** --> (3)**Personal access tokens** --> (4)**Generate new token**

![tokens配置](figures/gitee_webhook_01_03.png)

* 编辑 (5)**Token description**（类似Token名），修改权限范围至图示(6)，**当前 comment 功能仅需要“notes”权限**，并(7)**commit** 提交， 会提示输入密码：

![输入密码验证](figures/gitee_webhook_01_04.png)

* 请在点击 **Confirm and close** 之前**保存**好上面的token(只会显示一次，丢失了需要重新生成，你的配置文件json需要更新并提交 PR )。

![显示token](figures/gitee_webhook_01_05.png)
